FROM ru.net.oz/pizza-nodejs:latest AS Builder
LABEL maintainer="Valentin Burchenya <vp.burchenya@oz.net.ru>"


RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app

COPY . .
RUN npm install
RUN npm run build


#### Next section ####

FROM ru.net.oz/pizza-nodejs:latest
LABEL maintainer="Valentin Burchenya <vp.burchenya@oz.net.ru>"


HEALTHCHECK --interval=5s --timeout=3s --retries=3 CMD wget -q -O- http://localhost:9090/api/v1/ping | grep PONG || exit 1

RUN mkdir -p /usr/local/app/
WORKDIR /usr/local/app

# COPY --from=Builder /usr/src/app/node_modules ./node_modules/
COPY --from=Builder /usr/src/app/dist ./dist/
COPY --from=Builder /usr/src/app/MockApi*.js ./
COPY --from=Builder /usr/src/app/entrypoint.sh /ep.sh
RUN chmod +x /ep.sh

RUN npm install express uuid

USER nobody
EXPOSE 9090
ENTRYPOINT ["/ep.sh"]
CMD [ "node", "MockApiApp.js" ]
