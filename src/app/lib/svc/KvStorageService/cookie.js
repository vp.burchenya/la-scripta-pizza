import VueCookies from 'vue-cookies'


export default class {
    get(key) {
        return JSON.parse(VueCookies.get(key))
    }
    set(key, val) {
        VueCookies.set(key, JSON.stringify(val), Infinity)
    }
}
