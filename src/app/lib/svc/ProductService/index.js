import BaseUaService from '../BaseUaService'
import { Pizza } from '../../model'


export default class ProductService extends BaseUaService {
    // TODO: some urls should be /uid/basket, /uid/rate

    async getProducts() {
        let { data } = await this.ua.get('/products')
        return data.reduce(
            (acc, product) => {
                acc[product.product_id] = Object.assign(new Pizza, product)
                return acc
            }, {}
        )
    }
    async getBasket(uid) {
        return (await this.ua.get(`/basket/${uid}`)).data
    }
    async deleteBasket(uid) {
        await this.ua.delete(`/basket/${uid}`)
        return {}
    }
    async putBasket(uid, pid, count) {
        await this.ua.put(`/basket/${uid}/${pid}/${count}`)
    }
    async rateProduct(uid, pid, rate) {
        await this.ua.post(`/rate/${uid}`, { product_id: pid, rate: rate })
    }
}
