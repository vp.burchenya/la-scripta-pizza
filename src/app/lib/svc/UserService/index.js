import BaseUaService from '../BaseUaService'
import { User } from '../../model'


export default class UserService extends BaseUaService {

    async login(uid) {
        let qs
        if(uid == undefined)
            qs = 'guest'
        else
            qs = `uid=${uid}`

        let { data } = await this.ua.post('/login?' + qs)

        return Object.assign(new User, data)
    }

}
