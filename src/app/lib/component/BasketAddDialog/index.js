'use strict'


import { roundPrice as round } from '@/util'


export default {
    name: 'BasketAddDialog',
    props: [
        'product',
        'limit'
    ],
    data: () => ({
        'img': undefined,
        'quantity': 1,
        rules: {
            required: value => (value % 1 === 0 && value > 0) || 'Required positive number'
        }
    }),
    computed: {
        display() {
            if (! _.isEmpty(this.product)) {
                this.img = `/img/${this.product.product_id}.jpg`
                this.quantity = 1
                return true
            } else
                return false
        },
        price() {
            return round(
                (this.product.offer || this.product.price) * this.quantity
            )
        }
    },
    methods: {
        addProduct() {
            this.$emit('product:add', this.quantity)
        },
        closeProduct() {
            this.$emit('product:close')
        },
        buyProduct() {
            this.$emit('product:buy', this.quantity)
        }
    }
}
