'use strict'


import XRatings from '@@/Ratings'


export default {
    name: 'Offer',
    components: {
        XRatings
    },
    props: [
        'product',
        'count',
        'rated'
    ],
    computed: {
        img: function() {
            return `/img/${this.product.product_id}.jpg`
        },
        likes: function() {
            return this.product.likes || []
        },
        scoreNum: function() {
            return this.likes.reduce((acc, x) => acc + x, 0)
        },
        scoreSum: function() {
            return Object.entries(this.likes).reduce((acc, [score, count]) => acc + (1+(+score))*count, 0)
        },
        scoreAvg: function() {
            return Math.round(
                this.scoreSum / (this.scoreNum || 1)
            )
        }
    },
    methods: {
        productAdd() {
            this.$emit('product:add', this.product)
        },
        rate(val) {
            if(! this.ratingReadonly)
                this.$emit('product:rate', this.product, val)
        }
    }
}
