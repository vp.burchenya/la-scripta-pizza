'use strict'


export default {
    name: 'BasketBuyDialog',
    props: [
        'basket',
        'price',
        'display'
    ],
    data: () => ({
        message: '',
        address: '',
        phone: '',
        rules: {
            nonempty: value => !! value.trim() || 'Required non empty string',
            phoneNumber: value =>
                !! value && /^(7|8)?\d{10}$/.test(value.replace(/[^\d]+/g, ''))
                || 'Require a valid mobile phone number'
        },
        valid: true
    }),
    computed: {
        disabled() {
            return _.isEmpty(this.basket) || ! this.valid
        }
    },
    methods: {
        buyBasket() {
            if(this.validateForm()) {
                // ({ message, address, phone } = this.$data) not to want work
                this.$emit(
                    'basket:order',
                    (({ message, address, phone }) => ({ message, address, phone }))(this.$data)
                )
            }
        },
        closeBasket() {
            this.$emit('basket:close')
        },
        validateForm() {
            return this.$refs.form.validate()
        },
        resetForm() {
            this.$refs.form.reset()
        },
        resetValidation() {
            this.$refs.form.resetValidation()
        }
    }
}
