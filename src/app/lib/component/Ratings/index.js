import { VRating } from 'vuetify/lib'  // 'vuetify/es5/components/VRating'
import Vue from 'vue'


export default Vue.extend({  // VRating.extend
    mixins: [VRating],
    methods: {
        createClickFn(i) {
            let self = this
            // this.constructor.superOptions
            // Evan answer's, but check the extends vs mixins - this.constructor.super.options.methods
            let superClickFn = VRating.options.methods.createClickFn.call(this, i)
            return (ev) => {
                let val = self.genHoverIndex(ev, i)
                this.$emit('rate', val)
                // Default behavior is: superClickFn(ev) and watch.internalValue
            }
        }
    }
})
