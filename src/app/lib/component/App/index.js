'use strict'


import Vue from 'vue'
import VueRouter from 'vue-router'
import ShoppingBasket from '@@/ShoppingBasket/index.vue'
import Offers from '@@/Offers/index.vue'
import BasketView from '@@/BasketView/index.vue'
import { roundPrice as round } from '@/util'


export default {
    name: 'App',
    props: {},
    components: {
        [`X${ShoppingBasket.name}`]: ShoppingBasket,
        [`X${Offers.name}`]: Offers
    },
    router: new VueRouter({
        routes: [
            { path: '/', name: 'index', component: Offers, props: {} },
            { path: '/basket/', name: 'basket', component: BasketView }
        ]
    }),
    data: () => ({
        showNav: null,
        showThx: false,
        title: 'La Scripta Pizza Magazine',
        products : {},
        basket: {
            items: {},
            limit: 5
        },
        error: '',
        uid: null,
        likes: {},
    }),
    computed: {
        quantity: function() {
            return Object.values(this.basket.items).reduce(
                (acc, count) => acc + count, 0
            )
        },
        offerPrice: function() {
            let price = Object.entries(this.basket.items).reduce(
                (acc, [product_id, count]) => {
                    let product = this.products[product_id]
                    return acc + (product.offer || product.price) * count
                }, 0
            )
            return round(price)
        },
        realPrice: function() {
            let price = Object.entries(this.basket.items).reduce(
                (acc, [product_id, count]) => {
                    let product = this.products[product_id]
                    return acc + product.price * count
                }, 0
            )
            return round(price)
        },
        weight: function() {
            let weight = Object.entries(this.basket.items).reduce(
                (acc, [product_id, count]) => {
                    let product = this.products[product_id]
                    return acc + product.weight * count
                }, 0
            )
            return round(weight)
        }
    },
    mounted: async function() {
        let self = this

        // this.$bus.$on('offers:fetch', (data, resolve, reject) => {})
        this.$bus.$on('error', (err) => { self.error = err.message })
        
        async function reload() {
            try {
                // { uid: this.uid, likes: this.likes } = await this.login()
                let { uid, likes } = await this.login()
                this.uid = uid
                this.likes = likes

                this.products = await self.$productSvc.getProducts()
                this.basket.items = await self.$productSvc.getBasket(this.uid)
            } catch(ex) {
                console.error(ex)
                let code = ex.response && ex.response.status || 'system'
                self.$bus.$emit('error', new Error(code))
            }
        }

        this.$on('reload', reload)
        this.$emit('reload')
    },
    methods: {
        async login() {
            try {
                return await this.$userSvc.login(this.uid)
            } catch({ response }) {
                // Enter as guest if system isnt known us
                if(response.status == 401)
                    return await this.$userSvc.login()
            }
        },
        productBasketBuy() {
            this.$router.push({ name: 'basket', params: {}})
        },
        async productBasketAddId(product_id, requestedQuantity = 1) {
            let basketQuantity = this.basket.items[product_id] || 0
            let limit = Math.min(
                this.products[product_id].limit || Infinity,
                this.basket.limit || Infinity
            )
            if (basketQuantity < limit) {
                Vue.set(
                    this.basket.items,
                    product_id,
                    basketQuantity + Math.min(limit - basketQuantity, requestedQuantity)
                )

                await this.$productSvc.putBasket(this.uid, product_id, this.basket.items[product_id])
            }
        },
        async productBasketDelId(product_id) {
            if (! -- this.basket.items[product_id])
                this.productBasketCloseId(product_id)
            else
                await this.$productSvc.putBasket(this.uid, product_id, this.basket.items[product_id])
        },
        async productBasketCloseId(product_id) {
            Vue.delete(this.basket.items, product_id)
            await this.$productSvc.putBasket(this.uid, product_id, 0)
        },
        async basketOrder({message, address, phone}) {
            this.basket.items = await this.$productSvc.deleteBasket(this.uid)
            this.showThx = true
        },
        async productRate(product_id, val) {
            if(! (product_id in this.likes)) {
                let ix = val - 1
                let product = this.products[product_id]

                await this.$productSvc.rateProduct(this.uid, product_id, val)

                Vue.set(this.likes, product_id, val)
                Vue.set(product.likes, ix, (product.likes[ix] || 0) + 1)
            }
        }
    },
    beforeDestroy: function() {
        // No memleak: this.$bus.$listeners.$off
    },
    watch: {
        '$route'(to, from) {},
        uid: {
            // Maybe computed property is better
            handler(newValue) {
                if(newValue != undefined)
                    this.$kv.set('uid', newValue)
                else
                    this.uid = this.$kv.get('uid')
            },
            immediate: true
        }
    }
}
