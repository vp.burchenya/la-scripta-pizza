'use strict'


import Offer from '../Offer/index.vue'
import BasketAddDialog from '../BasketAddDialog/index.vue'


const EMPTY = {}

export default {
    name: 'Offers',
    props: [
        'products',
        'basket',
        'limit',
        'likes'
    ],
    data: () => ({
        product: EMPTY
    }),
    computed: {
        quantityLimit() {
            return Math.min(this.limit, this.product.limit || Infinity)
        }
    },
    components: {
        [`X${Offer.name}`]: Offer,
        [`X${BasketAddDialog.name}`]: BasketAddDialog
    },
    methods: {
        productDebug(product) {
            console.debug(JSON.stringify(product))
        },
        productAdd(product) {
            if (product.product_id in this.basket)
                this.$emit('product:basketAddId', product.product_id)
            else
                this.product = product
        },
        productCancel() {
            this.product = EMPTY
        },
        productBasketAdd(quantity) {
            this.$emit('product:basketAddId', this.product.product_id, quantity)
            this.product = EMPTY
        },
        productBasketBuy(quantity) {
            this.productBasketAdd(quantity)
            this.$emit('product:basketBuy')
        },
        productRate(product, val) {
            this.$emit('product:rate', product.product_id, val)
        }
    }
}
