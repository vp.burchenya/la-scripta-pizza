'use strict'


import BasketBuyDialog from '@@/BasketBuyDialog/index.vue'
import { roundPrice as round } from '@/util'


export default {
    name: 'BasketView',
    props: [
        'basket',
        'products',
        'offerPrice',
        'realPrice',
        'quantity',
        'weight'
    ],
    components: {
        [`X${BasketBuyDialog.name}`]: BasketBuyDialog
    },
    data: () => ({
        showBuy: false
    }),
    computed: {
        basketView: function() {
            return Object.entries(this.basket).map(
                ([product_id, count]) => {
                    let product = this.products[product_id]
                    console.assert(product)
                    return {
                        ... product,
                        count,
                        image_url: `/img/${product.product_id}.jpg`
                    }
                }
            )
        },
        discount() {
            return round(this.offerPrice - this.realPrice)
        }
    },
    methods: {
        round(price) {
            return round(price)
        },
        addProduct(product) {
            this.$emit('product:basketAddId', product.product_id)
        },
        delProduct(product) {
            this.$emit('product:basketDelId', product.product_id)
        },
        closeProduct(product) {
            this.$emit('product:basketCloseId', product.product_id)
        },
        basketOrder(orderData) {
            this.$emit('basket:order', orderData)
            this.showBuy = false
            this.$router.push({name: 'index'})
        }
    }
}
