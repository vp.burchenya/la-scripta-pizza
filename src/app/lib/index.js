'use strict'


import 'babel-polyfill'  // IE11 & Safari9

import Vue from 'vue'
import Vuetify from '@P/vuetify'
import App from '@@/App/index.vue'

import ua from '@P/axios'
import '@P/router'
import '@P/devtools'

import ProductService from '@S/ProductService'
import UserService from '@S/UserService'
import { CookieKvStorage } from '@S/KvStorageService'

import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/dist/vuetify.min.css'


Object.defineProperty(
    Vue.prototype,
    '$bus',
    {
        get: function() {
            return this.$root.bus
        }
    }
)

Vue.prototype.$productSvc = new ProductService(ua)
Vue.prototype.$userSvc = new UserService(ua)
Vue.prototype.$kv = new CookieKvStorage()

const vm = new Vue({
    vuetify: new Vuetify({}),
    render: h => h(App),
    data: () => ({
        bus: new Vue({})
    })
}).$mount('#app')

window.vm = vm
