import Product from './product'


export default class Pizza extends Product {
    /**
     * 
     * @param {(number|string)} product_id - Unique id (UUID) of the product
     * @param {string} title  - The pizza title
     * @param {string} [description] - The pizza description
     * @param {number} width  - The pizza width
     * @param {number} price - Price of the pizza
     * @param {number} [offer] - Offer price of the pizza
     * @param {number} weight - Weight
     * @param {string[]} [tags] - Pizza tags
     * @param {number[]} [likes] - List of the pizza scores
     */
    constructor(product_id, title, description=null, width, price, offer=null, weight, tags=[], likes=[]) {
        super(product_id, title, description, price, offer, weight, tags)
        this.width = width
        this.likes = likes
    }
}
