'use strict'


export default class Product {
    /**
     * 
     * @param {(number|string)} product_id - Unique id (UUID) of the product
     * @param {string} title  - The product title
     * @param {string} [description] - The product description
     * @param {number} price - Price
     * @param {number} [offer] - Offer price
     * @param {number} weight - Weight
     * @param {string[]} [tags] - Product tags
     * @param {number} [limit] - Quantity limit
     */
    constructor(product_id, title, description=null, price, offer=null, weight, tags=[], limit=10) {
        this.product_id = product_id
        this.title = title
        this.description = description
        this.price = price
        this.offer = offer
        this.tags = tags
        this.weight = weight
        this.limit = limit
    }
}
