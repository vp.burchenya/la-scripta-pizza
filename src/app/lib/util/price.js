export const roundPrice = (price) => Math.ceil(price * 100) / 100
