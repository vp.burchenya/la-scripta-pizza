import Vue from 'vue'
import Axios from 'axios'

import Config from '@/config.json'


console.debug(Config)

/*
  Another ways:
    Vue.prototype.$ua = Axios
    Axios.defaults.baseURL = '/api/v1/'
*/
const ua = Axios.create({
  baseURL: Config.baseURL || '/api/v1'
})

Vue.prototype.$ua = ua

export default ua
