const express = require('express')
const { v4: uuid4 } = require('uuid')


module.exports = (app) => {
    let router = express.Router()

    let userRepository = {}  // Or we can use app.users

    let products = {
        1  : { title: 'The King Pizza', description: 'King of the size',            width: 80, price: 9.99, weight: 1.2, likes: [8, 0, 19, 1, 5] },
        18 : { title: 'Pizza Mario',    description: 'Mario likes it',              width: 25, price: 4.55, offer: 0.91, weight: 0.5 },
        21 : { title: 'Fitness Pizza',  description: 'Say No! to fat',              width: 35, price: 8.50, weight: 0.7, likes: [0, 1, 0, 1, 0] },
        5  : { title: 'Pepperoni',      description: 'Recipe from Italy',           width: 35, price: 8.53, weight: 0.7 },
        10 : { title: 'Gavay',          description: 'Gavay fresh',                 width: 40, price: 9.99, offer: 0.90, weight: 1.0 },
        8  : { title: 'Cheeseburger',   description: 'Cheese, onion and cucumbers', width: 35, price: 5.3,  offer: 4, weight: 0.7 },
        4  : { title: 'Cheezy',         description: 'A lot of cheese',             width: 35, price: 5,    offer: 4.99, weight: 0.7, likes: [0, 0, 1, 1, 3] },
        11 : { title: 'Sea Pizza',      description: 'Seafood are waiting for you', width: 35, price: 7.5,  offer: 7, weight: 0.7 },
    }

    router.route('/ping').all((_, res) => {
        res.send('PONG')
    })

    router.route('/login').post((req, res) => {
        let isGuest = req.query.guest
        let user

        if(isGuest === '')
            isGuest = true
        else {
            isGuest = (isGuest || '').toLowerCase()
            isGuest = isGuest !== '' && (isGuest != '0' && isGuest != 'false')
        }

        if(!! req.query.uid) {
            user = userRepository[req.query.uid]
        } else if(isGuest) {
            let uuid = uuid4()
            user = userRepository[uuid] = {
                uid: uuid,
                likes: {},
                basket: {}
            }
        }

        if(! user)
            res.sendStatus(401)
        else
            res.send(user)
    })

    router.route('/basket/:bid').get((req, res) => {
        let basket = (userRepository[req.params.bid] || {}).basket

        if(basket)
            res.send(basket)
        else
            res.status(404).send('Not Found')
    })

    router.route('/basket/:bid').delete((req, res) => {
        let user = userRepository[req.params.bid]

        if(user) {
            user.basket = {}
            res.send('Deleted')
        } else
            res.status(404).send('Not Found')
    })

    router.route('/basket/:bid/:pid/:count').put((req, res) => {
        let bid = req.params.bid
        let pid = req.params.pid
        let count = Math.abs(Math.ceil(+req.params.count))

        if(isNaN(count))
            res.status(400).send('Bad Request')
        else {

            let user = userRepository[req.params.bid]

            if(! user)
                res.status(404).send('Not Found')
            else {
                user.basket = {
                    ... user.basket,
                    [pid]: count
                }

                if(! count) {
                    delete user.basket[pid]
                    res.status(201).send('Deleted')
                } else
                    res.status(201).send('Updated')
            }
        }
    })

    router.route('/products').get((_, res) => {
        res.send(
            Object
            .entries(products)
            .reduce((acc, [product_id, product]) => [
                    ... acc,
                    { ... product, product_id: product_id }
                ], []
            )
        )
    })

    router.route('/rate/:uid').post((req, res) => {
        let user = userRepository[req.params.uid]

        if(! user)
            res.status(404).send('Not Found')
        else {
            let { product_id, rate: vote } = req.body
            if(product_id in (user.likes || {}))
                res.status(400).send('Already voted')
            else {
                user.likes[product_id] = vote
                let product = products[product_id]
                if(! product)
                    res.status(404).send('Not Found')
                else {
                    let ix = vote - 1
                    ix = Math.min(4, ix)
                    ix = Math.max(0, ix)
                    product.likes = product.likes || []
                    product.likes[ix] = (product.likes[ix] || 0) + 1
                    res.status(201).send('Voted')
                }
            }
        }
    })

    return router
}
