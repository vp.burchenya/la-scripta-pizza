'use strict'


const Webpack = require('webpack')
const Path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MockApiRouter = require('./MockApiRouter')
const express = require('express')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin


module.exports = {
    mode: 'development',
    devtool: 'source-map',  // eval-source-map cheap-module-source-map
    entry: {
        'index': './src/app/lib/index'
    },
    output: {
        path: Path.resolve(__dirname, 'dist'),
        filename: 'lib/[name].js'
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',  // By default esm runtime does not include template compiler
            '@' : Path.join(__dirname, 'src/app/lib/'),
            '@P': Path.join(__dirname, 'src/app/lib/plugin'),
            '@@': Path.join(__dirname, 'src/app/lib/component'),
            '@M': Path.join(__dirname, 'src/app/lib/model'),
            '@S': Path.join(__dirname, 'src/app/lib/svc')
        }
    },
    plugins: [
        // new BundleAnalyzerPlugin(),
        new Webpack.ProvidePlugin({
            _: 'lodash',
            map: ['lodash', 'map'],
            // vue$: ['vue/dist/vue.esm.js', 'default']  // `default` is es2015 export
            // $: 'jquery',
            // jQuery: 'jquery',
            // 'window.jQuery': 'jquery'
        }),
        new CopyWebpackPlugin([
            { from: 'src/*.html', to: '[name].[ext]' },
            { from: 'src/img/*', to: 'img/[name].[ext]' },
            { from: 'favicon.*' }
        ]),
        new VueLoaderPlugin(),
        new VuetifyLoaderPlugin({ progressiveImages: true })  // or https://vuetifyjs.com/en/customization/a-la-carte/#manually-importing
    ],
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [{
                    loader: 'vue-loader'
                }]
            },
            {
                test: /\.(txt|tmpl.html)$/,
                use: [{
                    loader: 'raw-loader'
                }]
            },
            {
                test: /\.js$/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                '@babel/preset-env', {
                                    "modules": false,
                                    "targets": {
                                        "browsers": [
                                            "> 1%",
                                            "last 2 versions",
                                            "not ie <= 8"
                                        ]
                                    }
                                }
                            ]
                            // '@babel/preset-stage-2'
                        ],
                        plugins: [
                            '@babel/plugin-syntax-dynamic-import',
                            '@babel/plugin-proposal-optional-chaining',
                            // '@babel/plugin-transform-destructuring'
                        ]
                    }
                }],
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpeg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[hash:8].[ext]',
                        outputPath: 'assets/',
                        publicPath: '/img'
                    }
                }]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
                // test: /.*\.(eot|woff2?|ttf|svg)(\?(#\w+&)?v=\d+\.\d+\.\d+(#\w+)?)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'font/[name].[ext]?[hash:8]',
                    }
                }]
            },
            // Vuetify
            {
                test: /\.sass$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass'),
                            sassOptions: {
                                fiber: require('fibers'),
                                indentedSyntax: true
                            },                            
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                oneOf: [
                    {
                        resourceQuery: /module/,
                        use: [
                            'vue-style-loader',
                            {
                                loader: 'css-loader',
                                options: {
                                    modules: true,
                                    localIdentName: '[local]_[hash:base64:8]'
                                }
                            }
                        ]
                    },
                    // TODO: <template lang="css">
                    {
                        resourceQuery: /^\?vue/,
                        use: [
                            'vue-style-loader'
                        ]
                    },
                    {
                        use: [
                            'vue-style-loader',
                            'css-loader'
                        ]
                    }
                ]
            }
        ]
    },
    devServer: {
        contentBase: Path.resolve(__dirname, 'src'),
        // watchContenBase: true,
        host: '0.0.0.0',
        port: 9090,
        before: (app) => {
            app.use(express.json({}))
            app.use('/api/v1', MockApiRouter(app))
        },
        watchOptions: {
            ignored: [
                Path.resolve(__dirname, 'dist'),
                Path.resolve(__dirname, 'node_modules')
            ]
        }
    }
}


if (process.env.NODE_ENV == 'production') {
    module.exports.plugins = (
        module.exports.plugins || []
    ).concat([
        new Webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new Webpack.LoaderOptionsPlugin({
            minimize: true
        })        
    ])

    module.exports.optimization = {
        minimize: true
    }
}
