let express = require('express')
let router = require('./MockApiRouter')


let app = express()
app
  .use(express.json({}))
  .use('/api/v1', router(app))
  .use('/', express.static('dist'))
  .listen(9090)
