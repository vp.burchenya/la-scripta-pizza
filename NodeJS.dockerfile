FROM debian:buster-slim
LABEL maintainer="Valentin Burchenya <vp.burchenya@oz.net.ru>"


ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -qq
RUN apt-get install --no-install-recommends -qy \
    ca-certificates wget \
    gnupg dirmngr \
    && true

RUN echo "deb https://deb.nodesource.com/node_13.x buster main" >> /etc/apt/sources.list.d/postgresql.list
RUN wget -q -O- "https://deb.nodesource.com/gpgkey/nodesource.gpg.key" | apt-key add -

RUN apt-get update -qq
RUN apt-get install --no-install-recommends -qy nodejs

RUN apt-get purge -y --auto-remove gnupg dirmngr
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*
